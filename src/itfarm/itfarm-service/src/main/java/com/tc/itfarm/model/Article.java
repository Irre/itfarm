package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Article implements Serializable {
    private Integer recordId;

    private String title;

    private String titleImg;

    private Integer authorId;

    private Integer typeId;

    private String content;

    private String keyword;

    private Integer pageView;

    private Integer orderNo;

    private Date createTime;

    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    public Article(Integer recordId, String title, String titleImg, Integer authorId, Integer typeId, String content, String keyword, Integer pageView, Integer orderNo, Date createTime, Date modifyTime) {
        this.recordId = recordId;
        this.title = title;
        this.titleImg = titleImg;
        this.authorId = authorId;
        this.typeId = typeId;
        this.content = content;
        this.keyword = keyword;
        this.pageView = pageView;
        this.orderNo = orderNo;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
    }

    public Article() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getTitleImg() {
        return titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg == null ? null : titleImg.trim();
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword == null ? null : keyword.trim();
    }

    public Integer getPageView() {
        return pageView;
    }

    public void setPageView(Integer pageView) {
        this.pageView = pageView;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}