package com.tc.itfarm.service;

import java.util.Date;
import java.util.List;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Privilege;
import com.tc.itfarm.model.ext.PrivilegeTree;
import com.tc.itfarm.model.ext.Tree;

public interface PrivilegeService extends BaseService<Privilege> {

	/**
	 * 保存权限
	 * @param privilege
	 * @return
	 */
	Integer save(Privilege privilege);

	/**
	 * 根据roleId集合查询
	 * @param roleIds
	 * @return
	 */
	List<Privilege> selectByRoleId(List<Integer> roleIds);

	/**
	 * 根据roleId查询
	 * @param roleId
	 * @return
	 */
	List<Privilege> selectByRoleId(Integer roleId);

	/**
	 * 分页查询
	 * @param name
	 * @param code
	 * @param startDate
	 * @param endDate
	 * @param page
	 * @return
	 */
	PageList<Privilege> selectByPageList(String name, String code, Date startDate, Date endDate, Page page);

	/**
	 * 查询权限树
	 * @return
	 */
	List<PrivilegeTree> selectPrivilegetree();

	/**
	 * 查询权限树 封装到Tree对象
	 * @return
	 */
	List<Tree> selectAllTree();
}
