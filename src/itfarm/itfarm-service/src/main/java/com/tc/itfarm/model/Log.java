package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Log implements Serializable {
    private Integer recordId;

    private String type;

    private String oldContent;

    private String newContent;

    private Integer userId;

    private String username;

    private Date createTime;

    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    public Log(Integer recordId, String type, String oldContent, String newContent, Integer userId, String username, Date createTime, Date modifyTime) {
        this.recordId = recordId;
        this.type = type;
        this.oldContent = oldContent;
        this.newContent = newContent;
        this.userId = userId;
        this.username = username;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
    }

    public Log() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getOldContent() {
        return oldContent;
    }

    public void setOldContent(String oldContent) {
        this.oldContent = oldContent == null ? null : oldContent.trim();
    }

    public String getNewContent() {
        return newContent;
    }

    public void setNewContent(String newContent) {
        this.newContent = newContent == null ? null : newContent.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}